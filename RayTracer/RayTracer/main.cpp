#include <cstdio>
#include <iostream>
#include "Vec4.h"
using namespace std;

int main() {
	int nx = 200;
	int ny = 100;

	freopen("output.ppm", "w", stdout);

	cout << "P3\n" << nx << " " << ny << "\n255\n";
	for (int j=ny - 1; j >= 0; j--) {
		for (int i = 0; i < nx; i++) {
			Vec4 p = Vec4( (float) i / nx, (float) j / ny,  0.2f);
			p = p*255.99f;
			cout <<(int) p.r() << " " << (int) p.g() << " " << (int) p.b() << "\n";
		}
	}
	fclose(stdout);
	return 0;
}