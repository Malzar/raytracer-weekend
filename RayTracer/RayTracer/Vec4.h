#ifndef VEC3H
#define VEC3H

#include <math.h>
#include <stdlib.h>
#include <iostream>

#pragma once
class Vec4 {
public:
    Vec4() { e[0] = e[1] = e[2] = e[3] = 0; }
    Vec4( float e0 ) { e[0] = e[1] = e[2] = e[3] = e0; }
    Vec4( float e0, float e1, float e2 ) { e[0] = e0; e[1] = e1; e[2] = e2; e[3] = 0; }
    Vec4( float e0, float e1, float e2, float e3 ) { e[0] = e0; e[1] = e1; e[2] = e2; e[3] = e3; }
    inline float x() const { return e[0]; }
    inline float y() const { return e[1]; }
    inline float z() const { return e[2]; }
    inline float w() const { return e[3]; }
    inline float r() const { return e[0]; }
    inline float g() const { return e[1]; }
    inline float b() const { return e[2]; }
    inline float a() const { return e[3]; }
    
    inline const Vec4& operator+() const { return *this; }
    inline Vec4 operator-() { return Vec4( -e[0], -e[1], -e[2], -e[3] ); }
    inline float operator[] ( int i )const { return e[i]; }

    Vec4& operator+=( const Vec4 &v2 );
    Vec4& operator-=( const Vec4 &v2 );
    Vec4& operator*=( const Vec4 &v2 );
    Vec4& operator/=( const Vec4 &v2 );
    Vec4& operator*=( const float t );
    Vec4& operator/=( const float t );
    Vec4 operator*( const float &t ) const;
    Vec4 operator/( const float &t ) const;

    float Length()const;

    Vec4 UnitVector( Vec4 v );
    float Dot( const Vec4 &v1, const Vec4 &v2 );
    float Squared_length() const;
    /*Vec4 Cross( const Vec4 &v1, const Vec4 &v2 );*/

private:
    float e[4];
};


#endif 
