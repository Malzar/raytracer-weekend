#include "Vec4.h"

Vec4& Vec4::operator+=( const Vec4 &v2 ) {
    e[0] += v2.e[0];
    e[1] += v2.e[1];
    e[2] += v2.e[2];
    e[3] += v2.e[3];
    return *this;
}

Vec4 & Vec4::operator-=( const Vec4 & v2 ) {
    e[0] -= v2.e[0];
    e[1] -= v2.e[1];
    e[2] -= v2.e[2];
    e[3] -= v2.e[3];
    return *this;
}

Vec4& Vec4::operator*=( const Vec4 &v2 ) {
    e[0] *= v2.e[0];
    e[1] *= v2.e[1];
    e[2] *= v2.e[2];
    e[3] *= v2.e[3];
    return *this;
}

Vec4& Vec4::operator/=( const Vec4 &v2 ) {
    e[0] /= v2.e[0];
    e[1] /= v2.e[1];
    e[2] /= v2.e[2];
    e[3] /= v2.e[3];
    return *this;
}

Vec4& Vec4::operator*=( const float t ) {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    e[3] *= t;
    return *this;
}

Vec4& Vec4::operator/=( const float t ) {
    float k = 1.0f / t;
    e[0] *= k;
    e[1] *= k;
    e[2] *= k;
    e[3] *= k;
    return *this;
}

Vec4 Vec4::operator*( const float &t ) const {
    return  Vec4( this->e[0] * t,
                  this->e[1] * t,
                  this->e[2] * t,
                  this->e[3] * t );
}

Vec4 Vec4::operator/( const float &t ) const {
    return  Vec4( this->e[0] / t,
                  this->e[1] / t,
                  this->e[2] / t,
                  this->e[3] / t );
}

float Vec4::Length() const {
    return sqrtf( e[0] * e[0] + e[1] * e[1] + e[2] * e[2] + e[3] * e[3] );
}

Vec4 Vec4::UnitVector( Vec4 v ) {
    return v;// / v.Length();
}

inline float Vec4::Squared_length() const {
    return (e[0] * e[0] + e[1] * e[1] + e[2] * e[2] + e[3] * e[3]);
}

inline float Vec4::Dot( const Vec4 &v1, const Vec4 &v2 ) {
    return v1.e[0] * v2.e[0] + v1.e[1] * v2.e[1] +
        v1.e[2] * v2.e[3] + v1.e[3] * v2.e[3];
}

//inline Vec4 Vec4::Cross( const Vec4 &v1, const Vec4 &v2 ) {
//    return Vec4();
//}